Programmer: Christian Koback
Course: ENEL 487
Assignment 1
Program: Complex number calculator
Source Code : Assign1CodeC++STDOutput.cpp

To compile and use, use the following in a linux-based terminal with gcc compiler.
g++ Assign1CodeC++STDOutput.cpp -o ComplexCalc
./ComplexCalc

Input to Program: (operator) (real part,1st number) (imaginary part,1st number) (real part,2nd number) (imaginary part,2nd number)
When the operator is incorrect, the program will wait for the user to try again.

NOTE: to quit/exit the program, enter Q as the operator.

Calculator Operations: 
	A-> Addition
	S-> Subtraction
	M-> Multiplication
	D-> Division

Output of Program: Displays/gives a real number in the form of =>   (real part) (sign of imaginary) j (imaginary part)