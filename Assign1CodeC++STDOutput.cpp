/**
Project:  Assignment 1
Course:	ENEL 487
Date:	September 23, 2016
Programmer: Christian Koback

Description:  This program is a complex calculator programmed in C++.


*/


#include <iostream>
#include <string>

using namespace std;

class Complex {
private:
	double realNum;
	double imagNum;
	bool errorOccurredReal;
	bool errorOccurredImag;
public:
	Complex() {
		realNum = 0.00;
		imagNum = 0.00;
		errorOccurredReal = false;
		errorOccurredImag = false;
	}
	Complex(double realNumIn, double imagNumIn) {
		realNum = realNumIn;
		imagNum = imagNumIn;
		errorOccurredReal = false;
		errorOccurredImag = false;
	}
	bool ImaginaryIsNeg() {
		if ((imagNum > 0) || (imagNum == 0)) {
			return false;
		}
		return true;
	}

	Complex& addComplex(Complex newNum) {
		realNum = newNum.realNum + realNum;
		imagNum = newNum.imagNum + imagNum;
		return *this;
	}
	Complex& subtractComplex(Complex newNum) {
		realNum = realNum - newNum.realNum;
		imagNum = imagNum - newNum.imagNum;
		return *this;
	}
	Complex& multiplyComplex(Complex newNum) {
		realNum = newNum.realNum *  realNum;
		imagNum = newNum.imagNum *  imagNum;
		return *this;
	}
	Complex& divideComplex(Complex newNum, float bufferForZero) {
		//check if the numbers are close enough with error to 0
		// if 0, then result is nan
		if ((newNum.realNum < bufferForZero) && (newNum.realNum
			< 0 - bufferForZero)) {
			errorOccurredReal = true;
		}
		if ((newNum.imagNum < bufferForZero) && (newNum.imagNum
			< 0 - bufferForZero)) {
			errorOccurredImag = true;
		}
		if (!errorOccurredReal) {
			realNum = realNum / newNum.realNum;
		}
		if (!errorOccurredImag) {
			imagNum = imagNum / newNum.imagNum;
		}
		return *this;
	}
	string FormatComplex() {
		string realPart;
		string imagPart;
		string sign;
		if (ImaginaryIsNeg()) {
			sign = "-";
		}
		else {
			sign = "+";
		}
		if (errorOccurredReal)
		{
			realPart = "nan";
		}
		if (errorOccurredImag)
		{
			imagPart = "nan";
		}
		if (!errorOccurredReal) {
			realPart = realNum;
		}
		if (!errorOccurredImag) {
			imagPart = imagNum;
		}
		return (realPart + " " + sign + " j " + imagPart);
	}
};


int main()
{
	// Max length of text input.
	const int MAXLEN = 80;
	const int REALIMAG_NUM_COUNT = 4;
	const double BUFFER_FOR_ZEROCHECK = 0.0000056;

	string expression;
	string operationToDo;
	double* arguments;
	arguments = new double[REALIMAG_NUM_COUNT];
	Complex results;

	int split = 0;
	int argNum = 0;

	cout << "Complex Calculator \n";
	cout << "Type a Letter to specify the arithmetic operator (A,S,M,D) \n";
	cout << "followed by two complex numbers expressed as pairs of doubles. \n";
	cout << "Type Q to quit. \n";

	do {
		cout << "Enter exp:  ";
		cin >> operationToDo;
		for (int i = 0; i < REALIMAG_NUM_COUNT; i++) {
			cin >> arguments[i];
		}
		operationToDo = toupper(operationToDo[0]);

		if (operationToDo.find("Q") != std::string::npos) {
			return 0;
		}
		else {
			Complex number1(arguments[0], arguments[1]);
			Complex number2(arguments[2], arguments[3]);

			if (operationToDo.find("A") != std::string::npos) {
				results = number1.addComplex(number2);
			}
			else if (operationToDo.find("S") != std::string::npos) {
				results = number1.subtractComplex(number2);
			}
			else if (operationToDo.find("M") != std::string::npos) {
				results = number1.multiplyComplex(number2);
			}
			else if (operationToDo.find("D") != std::string::npos) {
				results = number1.divideComplex(number2, BUFFER_FOR_ZEROCHECK);
			}

		}

		cout << "\n " + results.FormatComplex() + " \n";
	} while (operationToDo.find("Q") == std::string::npos);

}










